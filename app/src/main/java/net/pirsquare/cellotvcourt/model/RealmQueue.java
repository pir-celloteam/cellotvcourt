package net.pirsquare.cellotvcourt.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Nut on 12/7/16.
 */

public class RealmQueue extends RealmObject {
    @PrimaryKey
    public String queueId;

    public String queueCalled;

    public String getQueueId() {
        return queueId;
    }

    public void setQueueId(String queueId) {
        this.queueId = queueId;
    }

    public String getQueueCalled() {
        return queueCalled;
    }

    public void setQueueCalled(String queueCalled) {
        this.queueCalled = queueCalled;
    }
}
