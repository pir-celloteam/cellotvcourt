package net.pirsquare.cellotvcourt.model;

/**
 * Created by Nut on 12/8/16.
 */

public class QueueData {

    public String queueId;

    public QueueCalled queueCalled;

    public QueueData(String queueId, QueueCalled queueCalled) {
        this.queueId = queueId;
        this.queueCalled = queueCalled;
    }

    public String getQueueId() {
        return queueId;
    }

    public void setQueueId(String queueId) {
        this.queueId = queueId;
    }

    public QueueCalled getQueueCalled() {
        return queueCalled;
    }

    public void setQueueCalled(QueueCalled queueCalled) {
        this.queueCalled = queueCalled;
    }
}
