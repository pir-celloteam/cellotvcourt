package net.pirsquare.cellotvcourt.model;

import java.util.List;

/**
 * Created by Nut on 12/7/16.
 */

public class QueueCalled {


    /**
     * service : {"id":"MP4XDqKHOO","name":"Service 1"}
     * status : call
     * refCode : gY52bE8
     * actions : ["accepted","call","call","call","call"]
     * createdAt : {"$date":1481109669759}
     * remain : 0
     * group : A
     * index : 1
     * user : {"providers":{}}
     * capacity : 1
     * extras : {}
     */

    private ServiceBean service;
    private String status;
    private String refCode;
    private CreatedAtBean createdAt;
    private int remain;
    private String group;
    private int index;
    private UserBean user;
    private int capacity;
    private ExtrasBean extras;
    private List<String> actions;

    public ServiceBean getService() {
        return service;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRefCode() {
        return refCode;
    }

    public CreatedAtBean getCreatedAt() {
        return createdAt;
    }

    public int getRemain() {
        return remain;
    }

    public void setRemain(int remain) {
        this.remain = remain;
    }

    public String getGroup() {
        return group;
    }

    public int getIndex() {
        return index;
    }

    public UserBean getUser() {
        return user;
    }

    public int getCapacity() {
        return capacity;
    }

    public ExtrasBean getExtras() {
        return extras;
    }

    public List<String> getActions() {
        return actions;
    }

    public void setActions(List<String> actions) {
        this.actions.clear();
        this.actions.addAll(actions);
    }

    public static class ServiceBean {
        /**
         * id : MP4XDqKHOO
         * name : Service 1
         */

        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class CreatedAtBean {
        /**
         * $date : 1481109669759
         */

        private long $date;

        public long get$date() {
            return $date;
        }

        public void set$date(long $date) {
            this.$date = $date;
        }
    }

    public static class UserBean {
        /**
         * providers : {}
         */

        private ProvidersBean providers;

        public ProvidersBean getProviders() {
            return providers;
        }

        public void setProviders(ProvidersBean providers) {
            this.providers = providers;
        }

        public static class ProvidersBean {
        }
    }

    public static class ExtrasBean {
    }
}
