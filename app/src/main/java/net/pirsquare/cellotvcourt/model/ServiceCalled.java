package net.pirsquare.cellotvcourt.model;

/**
 * Created by Nut on 12/2/16.
 */

public class ServiceCalled {

    /**
     * name : Service 1
     * createdAt : {"$date":1480670000609}
     */

    private String name;
    private CreatedAtBean createdAt;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public CreatedAtBean getCreatedAt() {
        return createdAt;
    }

    public static class CreatedAtBean {
        /**
         * $date : 1480670000609
         */

        private long $date;

        public long get$date() {
            return $date;
        }

    }
}
