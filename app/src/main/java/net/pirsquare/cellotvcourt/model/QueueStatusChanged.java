package net.pirsquare.cellotvcourt.model;

import java.util.List;

/**
 * Created by kung on 12/7/16.
 */

public class QueueStatusChanged {


    /**
     * status : call
     * actions : ["accepted","call"]
     * remain : 0
     */

    private String status;
    private int remain;
    private List<String> actions;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRemain() {
        return remain;
    }

    public void setRemain(int remain) {
        this.remain = remain;
    }

    public List<String> getActions() {
        return actions;
    }

    public void setActions(List<String> actions) {
        this.actions = actions;
    }
}
