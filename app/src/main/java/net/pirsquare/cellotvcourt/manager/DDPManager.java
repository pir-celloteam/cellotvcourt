package net.pirsquare.cellotvcourt.manager;

import android.text.TextUtils;
import android.util.Log;

import com.orhanobut.logger.Logger;

import net.pirsquare.cellotvcourt.activity.MainActivity;
import net.pirsquare.cellotvcourt.model.QueueAction;
import net.pirsquare.cellotvcourt.model.QueueCalled;
import net.pirsquare.cellotvcourt.model.QueueData;
import net.pirsquare.cellotvcourt.model.QueueStatusChanged;
import net.pirsquare.cellotvcourt.utils.Constant;
import net.pirsquare.cellotvcourt.utils.GsonHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import im.delight.android.ddp.Meteor;
import im.delight.android.ddp.MeteorCallback;
import im.delight.android.ddp.SubscribeListener;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by kampolleelaporn on 3/28/16.
 */
public class DDPManager implements MeteorCallback {

    private MainActivity mainActivity;
    private static DDPManager mInstance;
    private static Meteor mMeteor;
    private static String TAG = "METERO";
    private String subscriptionId;


    private BehaviorSubject<Boolean> observeStatusMeteor;


    private ArrayList<Map<String, Object>> listStackQueue;
    private HashMap<String, Object> queueHashmap;

    private static ConnectServerListener listener;
    private DataChangeListener dataChangeListener;

    private CompositeSubscription observable;
    private boolean isConnect;
    private ReconnectListener reconnectListener;
    private boolean isClose;

    public void init(MainActivity context, ConnectServerListener mListener) {
        this.mainActivity = context;
        listener = mListener;
        Meteor.setLoggingEnabled(true);
        mMeteor = new Meteor(context, Constant.METEOR_SERVER_URL);
        mMeteor.addCallback(mInstance);
        mMeteor.connect();
    }

    public void setDataChangeListener(DataChangeListener dataChangeListener) {
        this.dataChangeListener = dataChangeListener;
    }

    public static DDPManager getInstance() {
        if (mInstance == null)
            mInstance = new DDPManager();
        return mInstance;
    }


    public BehaviorSubject<Boolean> subscribeStatus() {
        BehaviorSubject<Boolean> observe = BehaviorSubject.create(mMeteor.isConnected());
        observe.subscribeOn(Schedulers.io())
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread());
        observeStatusMeteor = observe;
        return observe;
    }

    public void postStatusMeteor(boolean status) {
        if (observeStatusMeteor != null)
            observeStatusMeteor.onNext(status);
    }


    public boolean isConnect() {
        return isConnect;
    }


    @Override
    public void onConnect(boolean signedInAutomatically) {
        isConnect = true;
        Log.e(TAG, "Connected");
        Log.e(TAG, "Is logged in: " + mMeteor.isLoggedIn());
        Log.e(TAG, "User ID: " + mMeteor.getUserId());
        if (signedInAutomatically) {
            if (observable != null) {
                observable.unsubscribe();
            }
        }

        if (reconnectListener != null) {
            reconnectListener.reconnectSuccess();
        }

        subscribe();

    }


    private void subscribe() {
        Map<String, Object> condition = new HashMap<>();
        condition.put("$in", Arrays.asList(QueueAction.ACCEPT.getValue(), QueueAction.CALL.getValue(), QueueAction.UPDATE.getValue()));

        Map<String, Object> param = new HashMap<>();
        param.put("status", condition);

        subscriptionId = mMeteor.subscribe("queues.list", new Object[]{param}, new SubscribeListener() {
            @Override
            public void onSuccess() {
                Log.e(TAG, "Subscript Success");
                if (listener != null)
                    listener.onSuccess();
                postStatusMeteor(true);
            }

            @Override
            public void onError(String s, String s1, String s2) {
                Log.e(TAG, "Subscript Fail");
                if (listener != null)
                    listener.onFail();
                postStatusMeteor(false);
            }
        });
    }

    public void onReconnect(ReconnectListener listener) {
        mMeteor.reconnect();
        this.reconnectListener = listener;

    }

    @Override
    public void onDisconnect() {
        Log.e(TAG, "Server Disconnect");
        isConnect = false;

        postStatusMeteor(false);

        if (!isClose) {
            if (listener != null)
                listener.onFail();
            if (reconnectListener != null) {
                reconnectListener.reconnectFail();
                reconnectListener = null;
            }
        }
    }

    @Override
    public void onException(Exception e) {
        e.printStackTrace();
        Log.e(TAG, "Exception " + e.getMessage() + " : ");
        mMeteor.disconnect();
        if (e != null) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDataAdded(String collectionName, String documentID, String newValuesJson) {
        Log.e(TAG, " added to <" + collectionName + "> in <" + documentID + ">");
        Logger.e(newValuesJson);

        QueueManager.getInstance().InsertQueue(documentID, newValuesJson);
    }

    @Override
    public void onDataChanged(String collectionName, String documentID, String updatedValuesJson, String removedValuesJson) {
        Log.e(TAG, "  changed in <" + collectionName + "> in <" + documentID + ">");
        Log.e(TAG, "    Updated: ");
        Logger.e(updatedValuesJson);
        Log.e(TAG, "    Removed: " + removedValuesJson);

        try {
            JSONObject json = new JSONObject(updatedValuesJson);
            QueueStatusChanged queueStatusChanged = GsonHelper.getGson().fromJson(updatedValuesJson, QueueStatusChanged.class);
            String queue = QueueManager.getInstance().FindQueueChanged(documentID);

            if (!TextUtils.isEmpty(queue)) {
                QueueCalled queueCalled = GsonHelper.getGson().fromJson(queue, QueueCalled.class);
                // update after claim
                if (json.has("remain")) {

                    if (queueCalled.getActions().get(queueCalled.getActions().size() - 1).equalsIgnoreCase("update")) {
                        queueCalled.setActions(queueStatusChanged.getActions());
                    }
                } else {
                    // call queue
                    if (queueCalled.getActions().get(queueCalled.getActions().size() - 1).equalsIgnoreCase("accepted")) {
                        queueCalled.setStatus(queueStatusChanged.getStatus());
                        queueCalled.setActions(queueStatusChanged.getActions());
                        QueueData queueData = new QueueData(documentID, queueCalled);

                        if (dataChangeListener != null)
                            dataChangeListener.onCallQueue(queueData);

                    } else if (queueCalled.getActions().get(queueCalled.getActions().size() - 1).equalsIgnoreCase("reserved")) {
                        // update queue reserve
                        queueCalled.setActions(queueStatusChanged.getActions());

                    } else if (queueCalled.getActions().get(queueCalled.getActions().size() - 1).equalsIgnoreCase("call") ||
                            queueCalled.getActions().get(queueCalled.getActions().size() - 1).equalsIgnoreCase("update")) {
                        // update queue call
                        queueCalled.setActions(queueStatusChanged.getActions());
                        QueueData queueData = new QueueData(documentID, queueCalled);

                        if (dataChangeListener != null)
                            dataChangeListener.onCallQueue(queueData);
                    }
                }

                QueueManager.getInstance().InsertQueue(documentID, GsonHelper.getGson().toJson(queueCalled, QueueCalled.class));

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onDataRemoved(String collectionName, String documentID) {
        Log.e(TAG, "Queue removed from <" + collectionName + "> in document <" + documentID + ">");
        QueueManager.getInstance().DeleteQueue(documentID);
    }

    public void close() {
        Log.e(TAG, "Meteor Closed");
        isClose = true;
        if (mMeteor.isConnected()) {
            mMeteor.disconnect();
            mMeteor.removeCallback(this);
        }
        if (observable != null)
            observable.unsubscribe();
    }

    public interface DataChangeListener {
        void onCallQueue(QueueData queueCalled);
    }

    public interface ConnectServerListener {
        void onSuccess();

        void onFail();
    }

    public interface ReconnectListener {
        void reconnectSuccess();

        void reconnectFail();
    }
}