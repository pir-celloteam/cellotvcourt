package net.pirsquare.cellotvcourt.manager;

import android.content.Context;

import com.orhanobut.logger.Logger;

import net.pirsquare.cellotvcourt.model.QueueCalled;
import net.pirsquare.cellotvcourt.model.RealmQueue;

import java.util.Collection;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Nut on 12/07/2016.
 */
public class QueueManager {

    public Realm realm;

    private static QueueManager instance;

    public static QueueManager getInstance() {
        if (instance == null)
            instance = new QueueManager();
        return instance;
    }

    private Context mContext;

    private QueueManager() {
        mContext = Contextor.getInstance().getContext();
    }

    public void init() {
        realm = Realm.getDefaultInstance();

    }

    public void InsertQueue(final String queueId, final String queueCalled) {
        final RealmQueue realmQueue = new RealmQueue();
        realmQueue.setQueueId(queueId);
        realmQueue.setQueueCalled(queueCalled);
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
//                RealmQueue realmQueue = bgRealm.createObject(RealmQueue.class);
//                realmQueue.setQueueId(queueId);
//                realmQueue.setQueueCalled(queueCalled);

                bgRealm.copyToRealmOrUpdate(realmQueue);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                // Transaction was a success.
                Logger.i("Insert Queue Success");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                // Transaction failed and was automatically canceled.
                Logger.i("Insert Queue Error");

            }
        });
    }

    public void DeleteQueue(final String queueId) {

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                RealmResults<RealmQueue> results = bgRealm.where(RealmQueue.class).equalTo("queueId", queueId).findAll();
                results.deleteAllFromRealm();
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                // Transaction was a success.
                Logger.i("Delete Queue Success");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                // Transaction failed and was automatically canceled.
                Logger.i("Delete Queue Error");

            }
        });
    }

    public String FindQueueChanged(String queueId) {
        String q = "";
        RealmQueue queueCalled = realm.where(RealmQueue.class).equalTo("queueId", queueId).findFirst();
        if (queueCalled != null)
            q = queueCalled.getQueueCalled();
        return q;
    }
}
