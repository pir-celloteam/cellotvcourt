package net.pirsquare.cellotvcourt.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.orhanobut.logger.Logger;

import net.pirsquare.cellotvcourt.R;
import net.pirsquare.cellotvcourt.adapter.ServiceAdapter;
import net.pirsquare.cellotvcourt.manager.DDPManager;
import net.pirsquare.cellotvcourt.manager.QueueManager;
import net.pirsquare.cellotvcourt.model.QueueCalled;
import net.pirsquare.cellotvcourt.model.QueueData;
import net.pirsquare.cellotvcourt.services.CallQueueVO;
import net.pirsquare.cellotvcourt.services.SOLATVAnnounceSoundPlayer;
import net.pirsquare.cellotvcourt.services.SOLATVAnnouncer;
import net.pirsquare.cellotvcourt.view.CustomSquareLayout;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static io.realm.log.RealmLog.debug;

public class MainActivity extends AppCompatActivity implements DDPManager.DataChangeListener, SOLATVAnnouncer.OnStartAnnounce {

    @BindView(R.id.layout_ss)
    FrameLayout layoutSplashScreen;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.circle_server)
    CustomSquareLayout circleServer;

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;

    private ArrayList<QueueData> listQueue;
    private boolean isReconnect;
    private SOLATVAnnouncer _SOLATVAnnouncer;
    private ArrayList<QueueData> waitingQueue;
    private Observable<Boolean> observableMeteorStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        QueueManager.getInstance().init();
        Toast.makeText(MainActivity.this, "Server Connecting", Toast.LENGTH_SHORT).show();
        listQueue = new ArrayList<>();
        waitingQueue = new ArrayList<>();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        mockData();

        connectMeteor();
    }

    private void connectMeteor() {
        if (DDPManager.getInstance() == null || !DDPManager.getInstance().isConnect())
            Observable.timer(2000, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<Long>() {
                        @Override
                        public void call(Long aLong) {
                            DDPManager.getInstance().init(MainActivity.this, new DDPManager.ConnectServerListener() {
                                @Override
                                public void onSuccess() {
                                    setLayout();

                                    layoutSplashScreen.animate().alpha(0.0f).setDuration(1000).setListener(new AnimatorListenerAdapter() {
                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            super.onAnimationEnd(animation);
                                            layoutSplashScreen.setVisibility(View.GONE);
                                        }
                                    }).start();

                                    setObservableStatus();

                                }

                                @Override
                                public void onFail() {
//                                    setLayout();

                                    reLoginMeteor();
                                }
                            });

                        }
                    });
    }

    private void setObservableStatus() {
        observableMeteorStatus = DDPManager.getInstance().subscribeStatus();
        observableMeteorStatus.delaySubscription(5000, TimeUnit.MILLISECONDS).subscribe(new Action1<Boolean>() {
            @Override
            public void call(Boolean aBoolean) {
                if (!aBoolean && !isReconnect) {
                    Log.e("Meteor", "Reconnect......");
                    isReconnect = true;
                    circleServer.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circle_background_red, null));

                    DDPManager.getInstance().onReconnect(new DDPManager.ReconnectListener() {
                        @Override
                        public void reconnectSuccess() {
                            isReconnect = false;
                            Logger.e("Reconnect Success");
                            Toast.makeText(MainActivity.this, "Reconnect Success", Toast.LENGTH_SHORT).show();
                            circleServer.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circle_background_green, null));

                        }

                        @Override
                        public void reconnectFail() {
                            isReconnect = false;
                            Logger.e("Reconnect Fail");
                            circleServer.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circle_background_red, null));

                        }
                    });
                    Logger.e("aBoolean : " + aBoolean + " isReconnect : " + isReconnect + " waiting server reconnect");
                }
            }
        });
    }

    private void setLayout() {
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ServiceAdapter(MainActivity.this, listQueue);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        circleServer.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circle_background_green, null));

        DDPManager.getInstance().setDataChangeListener(this);

        if (_SOLATVAnnouncer == null)
            _SOLATVAnnouncer = new SOLATVAnnouncer(this, this);

//        RealmResults<RealmQueue> results = QueueManager.getInstance().realm.where(RealmQueue.class).findAll();
//        if (results != null) {
//            for (RealmQueue realmQueue : results) {
//                Logger.d(realmQueue.getQueueId());
//            }
//        }
    }

    private void reLoginMeteor() {
        if (!isReconnect) {
            Log.e("Meteor", "Reconnect......");
            isReconnect = true;
            Observable.timer(5000, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<Long>() {
                        @Override
                        public void call(Long aLong) {
                            DDPManager.getInstance().onReconnect(new DDPManager.ReconnectListener() {
                                @Override
                                public void reconnectSuccess() {
                                    isReconnect = false;
                                    Logger.e("Reconnect Success");
                                }

                                @Override
                                public void reconnectFail() {
                                    isReconnect = false;
                                    Logger.e("Reconnect Fail");
                                    MaterialDialog.Builder builder = new MaterialDialog.Builder(MainActivity.this)
                                            .title("Disconnect")
                                            .content("Can not connect to server please try again later")
                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                    MainActivity.this.finishAffinity();
                                                }
                                            })
                                            .autoDismiss(false);
                                    MaterialDialog dialog = builder.build();
                                    dialog.show();
                                }
                            });
                        }
                    });
        }
    }


    private void mockData() {
        for (int i = 0; i < 5; i++) {
            QueueData queueData = new QueueData("", null);
            listQueue.add(queueData);
        }
    }

    @Override
    public void onCallQueue(QueueData queueData) {
        waitingQueue.add(queueData);
        callQueueAnnouncer(queueData);

    }

    @Override
    public void onStartPlaySound(QueueData queueData) {
        if (listQueue.size() != 0) {
            checkOverLapQueue(queueData.getQueueId());
        }
        // refresh UI
        listQueue.add(0, queueData);
        mAdapter.notifyDataSetChanged();
    }

    private void checkOverLapQueue(String id) {
        for (int i = 0; i < listQueue.size(); i++) {
            if (listQueue.get(i).getQueueId().equalsIgnoreCase(id)) {
                listQueue.remove(i);
                mAdapter.notifyItemRemoved(i);
            }
        }
    }

    private void callQueueAnnouncer(QueueData queueData) {
        if (_SOLATVAnnouncer == null)
            _SOLATVAnnouncer = new SOLATVAnnouncer(this, this);
        _SOLATVAnnouncer.callQueue(queueData, SOLATVAnnounceSoundPlayer.TH);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DDPManager.getInstance().close();
        QueueManager.getInstance().realm.close();
    }

}
