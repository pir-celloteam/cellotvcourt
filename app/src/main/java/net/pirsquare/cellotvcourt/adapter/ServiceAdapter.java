package net.pirsquare.cellotvcourt.adapter;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorListenerAdapter;

import net.pirsquare.cellotvcourt.R;
import net.pirsquare.cellotvcourt.activity.MainActivity;
import net.pirsquare.cellotvcourt.model.QueueCalled;
import net.pirsquare.cellotvcourt.model.QueueData;
import net.pirsquare.cellotvcourt.utils.ConvertDesity;
import net.pirsquare.cellotvcourt.utils.ScreenUtils;

import java.util.ArrayList;

/**
 * Created by Nut on 12/2/16.
 */

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.MyViewHolder> {
    private ArrayList<QueueData> listQueue;
    private MainActivity mainActivity;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ServiceAdapter(MainActivity mainActivity, ArrayList<QueueData> myDataSet) {
        this.mainActivity = mainActivity;
        this.listQueue = myDataSet;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView serviceNumber;
        TextView serviceName;
        View bgNumber;
        View bgName;
        View dummy;

        private MyViewHolder(View v) {
            super(v);
            serviceNumber = (TextView) v.findViewById(R.id.tv_service_number);
            serviceName = (TextView) v.findViewById(R.id.tv_service_name);
            bgNumber = v.findViewById(R.id.bg_service_number);
            bgName = v.findViewById(R.id.bg_service_name);
            dummy = v.findViewById(R.id.dummy_view);
        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.service_called, parent, false);
        float screenHeight = ScreenUtils.getInstance().getScreenHeight();

        ViewGroup.LayoutParams layoutParams = itemView.getLayoutParams();
        layoutParams.height = (int) (ConvertDesity.convertPixelsToDp(screenHeight, mainActivity) - 160) / 6;

        itemView.setLayoutParams(layoutParams);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        if (position == 0 && !TextUtils.isEmpty(listQueue.get(0).getQueueId())) {
            YoYo.with(Techniques.Flash).duration(2000).playOn(holder.serviceNumber);
            YoYo.with(Techniques.Flash).duration(2000).playOn(holder.serviceName);
            YoYo.with(Techniques.Flash).duration(2000).playOn(holder.bgName);
            YoYo.with(Techniques.Flash).duration(2000).playOn(holder.bgNumber);
            YoYo.with(Techniques.Flash).withListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    holder.serviceNumber.setTextColor(mainActivity.getResources().getColor(android.R.color.black));
                    holder.serviceName.setTextColor(mainActivity.getResources().getColor(android.R.color.black));
                    holder.bgName.setBackground(mainActivity.getResources().getDrawable(R.drawable.button_round_rect_default));
                    holder.bgNumber.setBackground(mainActivity.getResources().getDrawable(R.drawable.button_round_rect_default));

                }
            }).duration(4000).playOn(holder.dummy);
            holder.serviceNumber.setTextColor(mainActivity.getResources().getColor(android.R.color.white));
            holder.serviceName.setTextColor(mainActivity.getResources().getColor(android.R.color.white));
            holder.bgName.setBackground(mainActivity.getResources().getDrawable(R.drawable.button_round_rect));
            holder.bgNumber.setBackground(mainActivity.getResources().getDrawable(R.drawable.button_round_rect));
        }

        holder.serviceNumber.setText(listQueue.get(position).getQueueCalled() != null ?
                (listQueue.get(position).getQueueCalled().getGroup() + listQueue.get(position).getQueueCalled().getIndex()) : "");
        holder.serviceName.setText(listQueue.get(position).getQueueCalled() != null ?
                (listQueue.get(position).getQueueCalled().getService().getName()) : "");

    }

    @Override
    public int getItemCount() {
        return 5;
    }

}
