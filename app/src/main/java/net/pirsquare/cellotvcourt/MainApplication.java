package net.pirsquare.cellotvcourt;

import android.app.Application;

import com.orhanobut.logger.Logger;

import net.danlew.android.joda.JodaTimeAndroid;
import net.pirsquare.cellotvcourt.manager.Contextor;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by kung on 12/5/16.
 */

public class MainApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Contextor.getInstance().init(getApplicationContext());
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RobotoCondensed-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        Logger.init("court");
        JodaTimeAndroid.init(this);
        Realm.init(this);

//        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
//                .setDefaultFontPath("fonts/DB Helvethaica X Bd.ttf")
//                .setFontAttrId(R.attr.fontPath)
//                .build()
//        );

    }


}
