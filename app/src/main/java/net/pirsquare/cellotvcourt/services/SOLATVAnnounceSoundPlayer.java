package net.pirsquare.cellotvcourt.services;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.util.Log;

import com.orhanobut.logger.Logger;

import net.pirsquare.cellotvcourt.model.QueueCalled;
import net.pirsquare.cellotvcourt.model.QueueData;

import java.util.ArrayList;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

public class SOLATVAnnounceSoundPlayer {

    private static String TAG = SOLATVAnnounceSoundPlayer.class.getCanonicalName();

    private static String[] _atozs = new String[]{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
    private static String[] _1to9s = new String[]{"ed", "2", "3", "4", "5", "6", "7", "8", "9"};
    private static String[] _1to9sEN = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9"};
    private static String[] _11to19s = new String[]{"11", "12", "13", "14", "15", "16", "17", "18", "19"};
    private static String[] _10to90s = new String[]{"10", "20", "30", "40", "50", "60", "70", "80", "90"};
    private static String[] _100to900s = new String[]{"100", "200", "300", "400", "500", "600", "700", "800", "900"};

    final public static String TH = "th";
    final public static String EN = "en";

    private static Context _context;
    private RepeatListener repeatListener;

    public SOLATVAnnounceSoundPlayer(Context context, RepeatListener repeatListener) {
        this.repeatListener = repeatListener;
        _context = context;
    }

    private int getSoundResource(String _language, String name) {
        String filename = "raw/" + _language + "_" + name;
        int id = _context.getResources().getIdentifier(filename, null, _context.getPackageName());
        return id;
    }

    public void startAnnounce(QueueCalled queueCalled, String lang) {
        try {
            Logger.i("startAnnounce :" + queueCalled.getGroup() + queueCalled.getIndex());

            // tell them i'm announcing

            List<Integer> _tracks = new ArrayList<Integer>();


            // thai
            if (lang.equals(TH)) {
                // invite
                _tracks.add(getSoundResource(TH, "invite"));

                announceGroupInThai(_tracks, queueCalled.getGroup());
                announceNumInThai(_tracks, String.valueOf(queueCalled.getIndex()));

                //_tracks.add(getSoundResource(TH, "blank1s"));

//                // at counter
//                if (Integer.parseInt(callQueueVO.queue_counter()) > 0) {
//                    _tracks.add(getSoundResource(lang, "counter"));
//                    announceNumInThai(_tracks, callQueueVO.queue_counter());
//                }
                // ka + thanks
                _tracks.add(getSoundResource(lang, "ka"));
            }

            //_tracks.add(getSoundResource(TH, "thanks"));

            // silent hill
            //_tracks.add(getSoundResource(TH, "blank1s"));

            // eng
            else if (lang.equals(EN)) {
                announceInEng(_tracks, queueCalled.getGroup(), String.valueOf(queueCalled.getIndex()));
            }

            // play
            playSequence((ArrayList<Integer>) _tracks);
//            repeatListener.onRepeatCall();

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void announceGroupInThai(List<Integer> _tracks, String queue_group) {
        // a->z
        _tracks.add(getSoundResource(TH, _atozs[(int) queue_group.charAt(0) - 'A']));
    }

    private void announceNumInThai(List<Integer> _tracks, String queue_index) {
        // "1","2","3"
        String numString = queue_index;
        char[] numStrings = numString.toCharArray();

        if (numStrings.length == 3) {
            // 100
            _tracks.add(getSoundResource(TH, _100to900s[(int) numStrings[0] - '0' - 1]));

            // 10
            if ((int) numStrings[1] > '0')
                _tracks.add(getSoundResource(TH, _10to90s[(int) numStrings[1] - '0' - 1]));

            // 1
            if ((int) numStrings[2] > '0')
                _tracks.add(getSoundResource(TH, _1to9s[(int) numStrings[2] - '0' - 1]));
        } else if (numStrings.length == 2) {
            // 10
            _tracks.add(getSoundResource(TH, _10to90s[(int) numStrings[0] - '0' - 1]));

            // 1
            if ((int) numStrings[1] > '0')
                _tracks.add(getSoundResource(TH, _1to9s[(int) numStrings[1] - '0' - 1]));
        } else if (numStrings.length == 1) {
            // 1
            if (numStrings[0] == '1')
                _tracks.add(getSoundResource(TH, "1"));
            else {
                _tracks.add(getSoundResource(TH, _1to9s[(int) numStrings[0] - '0' - 1]));
            }
        }
    }

    private void announceInEng(List<Integer> _tracks, String queue_group, String queue_index) {

        // invite
        _tracks.add(getSoundResource(EN, "next"));

        // a->z
        _tracks.add(getSoundResource(EN, _atozs[(int) queue_group.charAt(0) - 'A']));

        // "1","2","3"
        String numString = queue_index;
        char[] numStrings = numString.toCharArray();

        if (numStrings.length == 3) {
            // 100
            _tracks.add(getSoundResource(EN, _100to900s[(int) numStrings[0] - '0' - 1]));

            if ((int) numStrings[1] > '0')
                if ((int) numStrings[1] > '1' || (int) numStrings[2] == '0')
                    // 10
                    _tracks.add(getSoundResource(EN, _10to90s[(int) numStrings[1] - '0' - 1]));

            // 1
            if (numStrings[1] == '1') {
                if (numStrings[2] > '0')
                    _tracks.add(getSoundResource(EN, _11to19s[(int) numStrings[2] - '0' - 1]));
            } else {
                if ((int) numStrings[2] > '0')
                    _tracks.add(getSoundResource(EN, _1to9sEN[(int) numStrings[2] - '0' - 1]));
            }


        } else if (numStrings.length == 2) {
            if ((int) numStrings[0] > '1' || (int) numStrings[1] == '0')
                // 10
                _tracks.add(getSoundResource(EN, _10to90s[(int) numStrings[0] - '0' - 1]));

            // 1
            if (numStrings[0] == '1') {
                if (numStrings[1] > '0')
                    _tracks.add(getSoundResource(EN, _11to19s[(int) numStrings[1] - '0' - 1]));
            } else {
                if ((int) numStrings[1] > '0')
                    _tracks.add(getSoundResource(EN, _1to9sEN[(int) numStrings[1] - '0' - 1]));
            }

        } else if (numStrings.length == 1) {
            // 1
            if (numStrings[0] == '1')
                _tracks.add(getSoundResource(EN, "1"));
            else {
                _tracks.add(getSoundResource(EN, _1to9sEN[(int) numStrings[0] - '0' - 1]));
            }
        }

        _tracks.add(getSoundResource(TH, "blank1s"));

        // a->z
        _tracks.add(getSoundResource(EN, _atozs[(int) queue_group.charAt(0) - 'A']));

        if (numStrings.length == 3) {
            // 100
            _tracks.add(getSoundResource(EN, _100to900s[(int) numStrings[0] - '0' - 1]));

            if ((int) numStrings[1] > '0')
                if ((int) numStrings[1] > '1' || (int) numStrings[2] == '0')
                    // 10
                    _tracks.add(getSoundResource(EN, _10to90s[(int) numStrings[1] - '0' - 1]));

            // 1
            if (numStrings[1] == '1') {
                if (numStrings[2] > '0')
                    _tracks.add(getSoundResource(EN, _11to19s[(int) numStrings[2] - '0' - 1]));
            } else {
                if ((int) numStrings[2] > '0')
                    _tracks.add(getSoundResource(EN, _1to9sEN[(int) numStrings[2] - '0' - 1]));
            }


        } else if (numStrings.length == 2) {
            if ((int) numStrings[0] > '1' || (int) numStrings[1] == '0')
                // 10
                _tracks.add(getSoundResource(EN, _10to90s[(int) numStrings[0] - '0' - 1]));

            // 1
            if (numStrings[0] == '1') {
                if (numStrings[1] > '0')
                    _tracks.add(getSoundResource(EN, _11to19s[(int) numStrings[1] - '0' - 1]));
            } else {
                if ((int) numStrings[1] > '0')
                    _tracks.add(getSoundResource(EN, _1to9sEN[(int) numStrings[1] - '0' - 1]));
            }

        } else if (numStrings.length == 1) {
            // 1
            if (numStrings[0] == '1')
                _tracks.add(getSoundResource(EN, "1"));
            else {
                _tracks.add(getSoundResource(EN, _1to9sEN[(int) numStrings[0] - '0' - 1]));
            }
        }

        _tracks.add(getSoundResource(EN, "thanks"));
    }

    private static int mCompleted = 0;
    private static MediaPlayer _mp;

    private void playSequence(final ArrayList<Integer> tracks) {
        if (_mp != null) {
            try {
                _mp.stop();
                _mp.release();
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }

            _mp = null;
            mCompleted = 0;
        }

        _mp = MediaPlayer.create(_context, tracks.get(0));

        _mp.setOnCompletionListener(new OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (mp == null)
                    return;

                try {
                    mCompleted++;
                    mp.reset();
                    if (mCompleted < tracks.size()) {
                        try {
                            AssetFileDescriptor afd = _context.getResources().openRawResourceFd(tracks.get(mCompleted));
                            if (afd != null) {
                                mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                                afd.close();
                                mp.prepare();
                                mp.start();
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e.toString());
                            e.printStackTrace();
                        }

                    } else if (mCompleted >= tracks.size()) {
                        mCompleted = 0;
                        mp.release();
                        mp = null;

                        repeatListener.onRepeatCall();
                        //mainClass.callback(_groupID);

                        // will loop
                    /*
                     * mCompleted = 0; try { AssetFileDescriptor afd =
					 * getResources().openRawResourceFd(tracks.get(mCompleted));
					 * if (afd != null) {
					 * mp.setDataSource(afd.getFileDescriptor(),
					 * afd.getStartOffset(), afd.getLength()); afd.close();
					 * mp.prepare(); mp.start(); } } catch (Exception ex) {
					 * ex.printStackTrace(); }
					 */
                    } else {
                        mCompleted = 0;
                        mp.release();
                        mp = null;
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
            }
        });

        _mp.start();
    }

    public interface RepeatListener {
        void onRepeatCall();
    }
}
