package net.pirsquare.cellotvcourt.services;

import android.content.Context;
import android.util.Log;

import net.pirsquare.cellotvcourt.model.QueueCalled;
import net.pirsquare.cellotvcourt.model.QueueData;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;


/**
 * Created by katopz on 2014-10-07.
 */
public class SOLATVAnnouncer implements SOLATVAnnounceSoundPlayer.RepeatListener {
    private static String TAG = SOLATVAnnouncer.class.getCanonicalName();

    ArrayList<QueueData> queuesOrder;
    QueueData qCalled;

    SOLATVAnnounceSoundPlayer _SOLATVAnnounceSoundPlayer;

    private static Context _context;
    private String language;
    private OnStartAnnounce startAnnounce;

    public SOLATVAnnouncer(Context context, OnStartAnnounce onStartAnnounce) {
        _context = context;
        this.startAnnounce = onStartAnnounce;
    }

    public void callQueue(QueueData queueData, String lang) {
        try {
            language = lang;
            // no line yet, init one
            if (queuesOrder == null)
                queuesOrder = new ArrayList<>();

            boolean isExist = false;
            if (queuesOrder.size() > 0) {
                for (int i = 0; i < queuesOrder.size(); i++) {
                    if (queuesOrder.get(i).getQueueId().equalsIgnoreCase(queueData.getQueueId())) {
                        isExist = true;
                        return;
                    }
                }
            }
            // add it
            if (!isExist) {
                queuesOrder.add(queueData);
            }

            // now call
            callingCurrentQueueVO(lang);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void callingCurrentQueueVO(String lang) {

        // has line?
        if (queuesOrder == null || queuesOrder.size() <= 0)
            return;

        // busy calling anyone now?
        if (qCalled != null)
            return;

        // not busy, pick one
        Iterator it = queuesOrder.iterator();

        // not any left to call
        if (!it.hasNext())
            return;

        // gimme one
        qCalled = (QueueData) it.next();
        it.remove();

        // now speak
        announceAndBlink(qCalled, lang);
    }


    private void announceAndBlink(QueueData queueData, String lang) {

        // play sound
        if (_SOLATVAnnounceSoundPlayer == null)
            _SOLATVAnnounceSoundPlayer = new SOLATVAnnounceSoundPlayer(_context, this);
        // now play
        try {
            _SOLATVAnnounceSoundPlayer.startAnnounce(queueData.getQueueCalled(), lang);
            startAnnounce.onStartPlaySound(queueData);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            // skip to next one
            callingCurrentQueueVO(lang);
        }

    }

    @Override
    public void onRepeatCall() {
        qCalled = null;

        if (queuesOrder.size() > 0) {
            callingCurrentQueueVO(language);
        }
    }

    public interface OnStartAnnounce {
        void onStartPlaySound(QueueData queueData);
    }
}
