package net.pirsquare.cellotvcourt.services;

/**
 * Created by katopz on 2014-10-07.
 */
public class CallQueueVO {

    public String queue_group() {
        return _queue_group;
    }

    String _queue_group;
//
//    public String queue_counter() {
//        return _queue_counter;
//    }
//
//    String _queue_counter;

    public String queue_index() {
        return _queue_index;
    }

    String _queue_index;


    public CallQueueVO(String queue_group, String queue_index) {
        _queue_group = queue_group;
        _queue_index = queue_index;
    }
}
