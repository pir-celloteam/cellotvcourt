package net.pirsquare.cellotvcourt.utils;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Nut on 12/27/2016.
 */

public class DestinationUtils {

    public static String counter = "ช่องบริการ";

    public static int getDestinationNumber(String destination) {
        int number = 0;
        if (!TextUtils.isEmpty(destination)) {
            if (destination.contains(counter)) {
                destination = destination.substring(counter.length());
                number = Integer.parseInt(destination);
            }
        }
        return number;
    }
}
